import lab01.example.model.AbstractBankAccount;
import lab01.example.model.AccountHolder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractSimpleBankAccountTest implements BankAccountTest {
    protected AccountHolder accountHolder;
    protected AbstractBankAccount bankAccount;

    @Override
    @Test
    public void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }

    @Override
    @Test
    public void testDeposit() {
        bankAccount.deposit(this.accountHolder.getId(), 100);
        assertEquals(100, bankAccount.getBalance());
    }

    @Override
    @Test
    public void testWrongDeposit() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.deposit(2, 50);
        assertEquals(100, bankAccount.getBalance());
    }

    @Override
    @Test
    public void testWithdraw() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(accountHolder.getId(), 70);
        assertEquals(30, bankAccount.getBalance());
    }

    @Override
    @Test
    public void testWrongWithdraw() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(2, 70);
        assertEquals(100, bankAccount.getBalance());
    }
}
