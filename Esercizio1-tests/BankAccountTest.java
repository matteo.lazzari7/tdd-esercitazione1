import org.junit.jupiter.api.Test;

public interface BankAccountTest {
    @Test
    void testInitialBalance();

    @Test
    void testDeposit();

    @Test
    void testWrongDeposit();

    @Test
    void testWithdraw();

    @Test
    void testWrongWithdraw();
}
