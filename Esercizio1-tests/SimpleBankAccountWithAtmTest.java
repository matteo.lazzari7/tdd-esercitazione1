import lab01.example.model.AccountHolder;
import lab01.example.model.SimpleBankAccount;
import lab01.example.model.SimpleBankAccountWithAtm;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class SimpleBankAccountWithAtmTest extends AbstractSimpleBankAccountTest {
    private SimpleBankAccountWithAtm simpleBankAccountWithAtm;

    @BeforeEach
    public void beforeEach() {
        this.accountHolder = new AccountHolder("Mario", "Rossi", 0);
        this.bankAccount = new SimpleBankAccountWithAtm(this.accountHolder, 0);
        this.simpleBankAccountWithAtm = (SimpleBankAccountWithAtm) this.bankAccount;
    }
    @Test
    void testDepositWithAtm() {
        simpleBankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccount.getBalance());
    }
    @Test
    void testWithdrawWithAtm() {
        simpleBankAccountWithAtm.deposit(accountHolder.getId(), 100);
        simpleBankAccountWithAtm.withdrawWithAtm(accountHolder.getId(), 50);
        assertEquals(49, bankAccount.getBalance());
    }
}
