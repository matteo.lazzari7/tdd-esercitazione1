package lab01.tdd;

public class EqualsStrategy implements SelectStrategy{
    private final int numberEqual;

    public EqualsStrategy(int numberEqual) {
        this.numberEqual = numberEqual;
    }

    @Override
    public boolean apply(int element) {
        return element == numberEqual;
    }
}
