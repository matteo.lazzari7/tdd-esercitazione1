package lab01.tdd;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

public class CircularListImplementation implements CircularList{
    private ArrayList<Integer> circularList = new ArrayList<Integer>();
    private int listIndex = 0;

    @Override
    public void add(int element) {
        this.circularList.add(element);
    }

    @Override
    public int size() {
        return this.circularList.size();
    }

    @Override
    public boolean isEmpty() {
        return this.circularList.size() == 0;
    }

    @Override
    public Optional<Integer> next() {

        if(this.circularList.isEmpty())
            return Optional.ofNullable(null);
        else
        {
            int listValue;
            if(this.listIndex >= this.circularList.size()) {
                listIndex = 0;
                listValue = this.circularList.get(this.listIndex);
            }
            else {
                listValue = this.circularList.get(this.listIndex);
                this.listIndex++;
            }
            return Optional.ofNullable(listValue);
        }
    }

    @Override
    public Optional<Integer> previous() {
        if(this.circularList.isEmpty())
            return Optional.ofNullable(null);
        else
        {
            int listValue;
            if(listIndex > 0) {
                listValue = this.circularList.get(this.listIndex);
                this.listIndex = listIndex - 1;
            }
            else {
                listValue = this.circularList.get(this.listIndex);
                this.listIndex = this.circularList.size() - 1;
            }
            return Optional.ofNullable(listValue);
        }
    }

    @Override
    public void reset() {
        this.listIndex = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if(this.circularList.isEmpty())
            return Optional.ofNullable(null);
        int actualListIndex = listIndex;
        for (int index = 0; index < circularList.size(); index++) {
            int optionalIntValue = next().get();
            if(strategy.apply(optionalIntValue))
            {
                listIndex = actualListIndex;
                return Optional.ofNullable(optionalIntValue);
            }
        }
        return Optional.ofNullable(null);
    }
}
