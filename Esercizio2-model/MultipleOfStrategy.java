package lab01.tdd;

public class MultipleOfStrategy implements SelectStrategy{
    private final int selectedMultiple;

    public MultipleOfStrategy(int selectedMultiple) {
        this.selectedMultiple = selectedMultiple;
    }

    @Override
    public boolean apply(int element) {
        return element % selectedMultiple == 0;
    }
}
