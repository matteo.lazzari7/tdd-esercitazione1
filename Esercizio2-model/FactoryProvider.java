package lab01.tdd;

public class FactoryProvider {
    public static AbstractFactory getFactory(){
        return new StrategyFactory();
    }
}
