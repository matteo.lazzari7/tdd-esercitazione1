package lab01.tdd;

public abstract interface AbstractFactory<T> {
    abstract SelectStrategy create(String strategyType);
    abstract SelectStrategy create(String strategyType, int selectedNumber);
}
