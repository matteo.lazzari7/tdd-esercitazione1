package lab01.tdd;

public class StrategyFactory implements AbstractFactory<SelectStrategy>{

    @Override
    public SelectStrategy create(String strategyType) {
        if("EvenStrategy".equalsIgnoreCase(strategyType))
            return new EvenStrategy();
        return null;
    }

    @Override
    public SelectStrategy create(String strategyType, int selectedNumber) {
        if("MultipleOfStrategy".equalsIgnoreCase(strategyType))
            return new MultipleOfStrategy(selectedNumber);
        if("EqualsStrategy".equalsIgnoreCase(strategyType))
            return new EqualsStrategy(selectedNumber);
        return null;
    }
}
