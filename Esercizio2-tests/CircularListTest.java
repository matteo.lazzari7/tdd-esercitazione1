import lab01.tdd.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {
    private CircularListImplementation circularListImplementation;
    private AbstractFactory strategyFactory;
    @BeforeEach
    public void beforeEach()
    {
        this.circularListImplementation = new CircularListImplementation();
        strategyFactory = FactoryProvider.getFactory();
    }

    @Test
    public void testAdd(){
        assertTrue(circularListImplementation.isEmpty());
        circularListImplementation.add(0);
        assertEquals(0, circularListImplementation.next().get());
    }

    @Test
    public void testEmpty(){
        assertTrue(circularListImplementation.isEmpty());
        circularListImplementation.add(0);
        assertFalse(circularListImplementation.isEmpty());
    }

    @Test
    public void testSize(){
        assertEquals(0, circularListImplementation.size());
        circularListImplementation.add(0);
        circularListImplementation.add(1);
        assertEquals(2, circularListImplementation.size());
    }

    @Test
    public void testNext() {
        assertEquals(Optional.empty(), circularListImplementation.next());
        circularListImplementation.add(1);
        circularListImplementation.add(2);
        circularListImplementation.add(3);
        assertEquals(1, circularListImplementation.next().get());
        assertEquals(2, circularListImplementation.next().get());
        assertEquals(3, circularListImplementation.next().get());
        assertEquals(1, circularListImplementation.next().get());
    }

    @Test
    public void testPrevius() {
        assertEquals(Optional.empty(), circularListImplementation.previous());
        circularListImplementation.add(1);
        circularListImplementation.add(2);
        circularListImplementation.add(3);
        assertEquals(1, circularListImplementation.previous().get());
        assertEquals(3, circularListImplementation.previous().get());
        assertEquals(2, circularListImplementation.previous().get());
        assertEquals(1, circularListImplementation.previous().get());
    }

    @Test
    public void testReset() {
        circularListImplementation.add(1);
        circularListImplementation.add(2);
        circularListImplementation.add(3);
        assertEquals(1, circularListImplementation.previous().get());
        assertEquals(3, circularListImplementation.previous().get());
        circularListImplementation.reset();
        assertEquals(1, circularListImplementation.previous().get());
        assertEquals(3, circularListImplementation.previous().get());
    }

    @Test
    public void testNextStrategyEven() {
        SelectStrategy selectStrategy = strategyFactory.create("EvenStrategy");
        circularListImplementation.add(1);
        circularListImplementation.add(3);
        circularListImplementation.add(5);
        assertEquals(Optional.empty(),circularListImplementation.next(selectStrategy));
        circularListImplementation.add(2);
        assertEquals(2,circularListImplementation.next(selectStrategy).get());
    }

    @Test
    public void testNextStrategyMultiple() {
        SelectStrategy selectStrategy = strategyFactory.create("MultipleOfStrategy", 2);
        circularListImplementation.add(1);
        circularListImplementation.add(3);
        circularListImplementation.add(5);
        assertEquals(Optional.empty(),circularListImplementation.next(selectStrategy));
        circularListImplementation.add(2);
        circularListImplementation.add(6);
        assertEquals(2,circularListImplementation.next(selectStrategy).get());
    }

    @Test
    public void testNextStrategyEquals() {
        SelectStrategy selectStrategy = strategyFactory.create("EqualsStrategy", 2);
        circularListImplementation.add(1);
        circularListImplementation.add(3);
        circularListImplementation.add(5);
        assertEquals(Optional.empty(),circularListImplementation.next(selectStrategy));
        circularListImplementation.add(2);
        assertEquals(2,circularListImplementation.next(selectStrategy).get());
    }
}
