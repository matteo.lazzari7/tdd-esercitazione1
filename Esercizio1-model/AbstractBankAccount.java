package lab01.example.model;

public abstract class AbstractBankAccount implements BankAccount{
    protected final AccountHolder holder;
    protected double balance;

    public AbstractBankAccount(AccountHolder holder, int startingBalance) {
        this.holder = holder;
        this.balance = startingBalance;
    }

    @Override
    public AccountHolder getHolder() {
        return holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void deposit(int userID, double amount) {
        if(checkUser(userID))
            this.balance = this.balance + amount;
    }

    @Override
    public void withdraw(int userID, double amount) {
        if(checkUser(userID) && isWithdrawAllowed(amount))
            this.balance = this.balance - amount;
    }

    protected boolean isWithdrawAllowed(final double amount){
        return this.balance >= amount;
    }

    protected boolean checkUser(final int userId) { return this.holder.getId() == userId; }

}
