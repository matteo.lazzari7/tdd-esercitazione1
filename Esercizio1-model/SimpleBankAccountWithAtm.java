package lab01.example.model;

public class SimpleBankAccountWithAtm extends AbstractBankAccount {
    private final int FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder accountHolder, int startingDeposit) {
        super(accountHolder, startingDeposit);
    }

    public void depositWithAtm(int userID, int amount) {
        if(checkUser(userID))
            this.balance = this.balance + amount - FEE;
    }

    public void withdrawWithAtm(int userID, int amount) {
        if(checkUser(userID) && isWithdrawAllowed(amount))
            this.balance = this.balance - (amount + FEE);
    }
}
