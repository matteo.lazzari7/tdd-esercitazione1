package lab01.example.model;

public class SimpleBankAccount extends AbstractBankAccount {

    public SimpleBankAccount(AccountHolder accountHolder, int startingDeposit) {
        super(accountHolder, startingDeposit);
    }
}
